ifeq ($(TARGET_GAPPS_ARCH),)
    $(error "GAPPS: TARGET_GAPPS_ARCH is undefined")
endif

ifneq ($(TARGET_GAPPS_ARCH),arm)
    ifneq ($(TARGET_GAPPS_ARCH),arm64)
        $(error "GAPPS: Only arm and arm64 are allowed")
    endif
endif

$(call inherit-product, vendor/gapps/common-blobs.mk)

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps

ifeq ($(IS_PHONE),true)
    PRODUCT_PACKAGES += \
        com.google.android.dialer.support

    PRODUCT_PACKAGES += \
        PrebuiltBugle

    PRODUCT_PACKAGES += \
        GoogleDialer
endif

ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
    PRODUCT_PACKAGES += \
        arcore
endif

# Recorder
ifeq ($(TARGET_SUPPORTS_GOOGLE_RECORDER), true)
    PRODUCT_PACKAGES += \
        RecorderPrebuilt
endif

# Core product app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    LatinIMEGooglePrebuilt \
    LocationHistoryPrebuilt \
    MarkupGoogle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail \
    SoundPickerPrebuilt \
    WebViewGoogle0

# Core product priv-app
PRODUCT_PACKAGES += \
    CarrierServices \
    GoogleOneTimeInitializer \
    GoogleServicesFramework \
    Phonesky \
    PixelSetupWizard \
    PrebuiltGmsCoreQt \
    TurboPrebuilt \
    SetupWizardPrebuilt \
    WallpaperPickerGoogleRelease \
    WellbeingPrebuilt

ifneq ($(strip $(CORE_GAPPS)), true)
    # System app
    PRODUCT_PACKAGES += \
        GoogleExtShared \
        GooglePrintRecommendationService

    # System priv-app
    PRODUCT_PACKAGES += \
        GoogleExtServicesPrebuilt

    # Product app
    PRODUCT_PACKAGES += \
        GoogleTTS \
        Photos \
        talkback

    # Product priv-app
    PRODUCT_PACKAGES += \
        ConnMetrics \
        GoogleFeedback \
        GooglePartnerSetup \
        Velvet
endif

ifeq ($(strip $(BUILD_GALLERYGO)), true)
ifeq ($(strip $(TARGET_GAPPS_ARCH)), arm64)
    PRODUCT_PACKAGES += \
        GalleryGo
endif
endif
